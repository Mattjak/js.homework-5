
function createNewUser() {
    let firstName = prompt("Enter your first name");
    let lastName = prompt("Enter your second name");
    let birthday = prompt("enter your birthday in format dd.mm.yyyy");
    let newUser = {
        usersFirstName : firstName,
        userLastName : lastName,
        birthday:    birthday,
        getAge: function () {
            const birthdayFormatted = birthday.split(".").reverse().join("-");
            let userBirthday = new Date(birthdayFormatted);
            let now = new Date();
            let ageMilliseconds  =  now - userBirthday;
            let ageOfUser = ageMilliseconds/(1000 * 3600 * 365 *24);
            return Math.floor(ageOfUser);
        },
        getPassword: function () {
            let array = birthday.split(".");
            let birthdayYear = array[2];
            return   this.usersFirstName[0].toUpperCase() + this.userLastName.toLowerCase() + birthdayYear;
        }
    };
    return newUser;
}
let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());